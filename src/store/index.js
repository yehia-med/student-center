// import vuex
import Vuex from "vuex";

// import modules

// Auth modules
import login from "./modules/auth/login.store";
import signup from "./modules/auth/signup.store";

// Courses modules
import englishCourseStore from "./modules/courses/englishCourse.store";
import universityCoursesStore from "./modules/courses/universityCourses.store";
import courseProfileStore from "./modules/courses/courseProfile.store";
import filters from "./modules/courses/filters";

// Pages modules
import accommodationsStore from "./modules/accommodations/accommodations.store";
import accommodationsFilterStore from "./modules/accommodations/accommodationsFilter.store";
import myAdsAccommodationsStore from "./modules/accommodations/myAdsAccommodations.store";
import SingleAccommodationsStore from "./modules/accommodations/SingleAccommodations.store";

import sellBuyStore from "./modules/sell-buy/sellBuy.store";
import sellBuyFilterStore from "./modules/sell-buy/sellBuyFilter.store";
import myAdsSellBuyStore from "./modules/sell-buy/myAdsSellBuy.store";
import SingleSellBuyStore from "./modules/sell-buy/SingleSellBuy.store";

import workshopsStore from "./modules/workshops/workshops.store";
import workshopsFilterStore from "./modules/workshops/workshopsFilter.store";
import myAdsWorkshopsStore from "./modules/workshops/myAdsWorkshops.store";
import SingleWorkShopsStore from "./modules/workshops/SingleWorkShops.store";

import QuestionsAnswersStore from "./modules/questions-answers/QuestionsAnswers.store";

// Profile modules
import changePassword from "./modules/profile/changePassword.store";
import editProfileStore from "./modules/profile/editProfile.store";
import applicationsStore from "./modules/profile/applications.store";
import documentsStore from "./modules/profile/documents.store";

// Home modules
import englishHomeCourseStore from "./modules/home/englisgPopularCourses.store";
import universityHomeCourseStore from "./modules/home/universityPopularCourses.store";
import sectionHomeStore from "./modules/home/sectionsHome.store";
import blogsStore from "./modules/blog/blog.store";
import aboutStore from "./modules/about/about.store";
import pagesStore from "./modules/pages/pages.store";

// import mixins
import user from "./modules/user.store";
import CreatePostStore from "./modules/mixins/CreatePost.store";

// create store
export default new Vuex.Store({
  modules: {
    // here we will add all the modules we have imported
    login,
    signup,
    user,
    englishCourseStore,
    universityCoursesStore,
    courseProfileStore,
    filters,
    englishHomeCourseStore,
    universityHomeCourseStore,
    sectionHomeStore,
    blogsStore,
    changePassword,
    editProfileStore,
    accommodationsStore,
    accommodationsFilterStore,
    myAdsAccommodationsStore,
    sellBuyStore,
    sellBuyFilterStore,
    myAdsSellBuyStore,
    workshopsStore,
    workshopsFilterStore,
    myAdsWorkshopsStore,
    aboutStore,
    CreatePostStore,
    SingleAccommodationsStore,
    SingleSellBuyStore,
    SingleWorkShopsStore,
    QuestionsAnswersStore,
    applicationsStore,
    documentsStore,
    pagesStore
  }
});
