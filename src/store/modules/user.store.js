// axios
import axios from "axios";
// router
import router from "@/router";

const state = {
   token: null,
   profile: null
};

// actions
const actions = {
  // attempt to user
  // eslint-disable-next-line no-unused-vars
  async attemptProfile({ commit }, token) {
    await axios
      .get("auth/profile", {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })
      // eslint-disable-next-line no-unused-vars
      .then((response) => {
        commit("getData", response.data.data);
      })
  },

  // attempt to logou
  // eslint-disable-next-line no-unused-vars
  async attemptLogout({ commit }, token) {
    await axios
      .get("auth/logout", {
        headers: {
          'Authorization': `Bearer ${token}`
        }
      })
      // eslint-disable-next-line no-unused-vars
      .then((response) => {
        localStorage.removeItem('token');
        router.push({ name: "Home" });
      })
  },

};

// mutations
const mutations = {
  setToken(state, token) {
    state.token = token;
  },

  getData(state, newData) {
    state.profile = newData;
  },
};


export default {
  namespaced: true,
  state,
  actions,
  mutations
};
