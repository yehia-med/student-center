// axios
import axios from "axios";

export default {
  namespaced: true,

  state: {
    EnglishPopularCourses: null,
  },

  getters: {
    getEnglishPopularCourses: (state) => state.EnglishPopularCourses
  },

  mutations: {
    ENGLISH_COURSES(state, value){
      state.EnglishPopularCourses = value
    }
  },

  actions: {
    async EnglishPopularCourses({commit}){
      axios.get(`/english-courses`)
          .then(res => commit("ENGLISH_COURSES",res.data.data))
    },
  }
}
