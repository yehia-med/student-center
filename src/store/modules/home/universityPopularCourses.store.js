// axios
import axios from "axios";

export default {
  namespaced: true,

  state: {
    UniversityPopularCourses: null,
  },

  getters: {
    getUniversityPopularCourses: (state) => state.UniversityPopularCourses
  },

  mutations: {
    UNIVERSITY_COURSES(state, value){
      state.UniversityPopularCourses = value
    }
  },

  actions: {
    async UniversityPopularCourses({commit}){
      axios.get(`/university-courses`)
          .then(res => commit("UNIVERSITY_COURSES",res.data.data))
    },
  }
}
