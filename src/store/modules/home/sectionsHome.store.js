// axios
import axios from "axios";

export default {
  namespaced: true,

  state: {
    sectionBanner: null,
    sectionWork: null,
    sectionBuild: null,
    sectionJourney: null,
    sectionSubjects: null,
    sectionFeedback: null,
    sectionAchievements: null,
    sectionBook: null,
    sectionWhatsapp: null,
    sections: null
  },

  getters: {
    getsectionBanner: (state) => state.sectionBanner,
    getsectionWork: (state) => state.sectionWork,
    getsectionBuild: (state) => state.sectionBuild,
    getsectionJourney: (state) => state.sectionJourney,
    getsectionSubjects: (state) => state.sectionSubjects,
    getsectionFeedback: (state) => state.sectionFeedback,
    getsectionAchievements: (state) => state.sectionAchievements,
    getsectionBook: (state) => state.sectionBook,
    getsectionWhatsapp: (state) => state.sectionWhatsapp,
    getsections: (state) => state.section
  },

  mutations: {
    SET_Banner(state, value) {
      state.sectionBanner = value
    },

    SET_Work(state, value) {
      state.sectionWork = value
    },

    SET_Build(state, value) {
      state.sectionBuild = value
    },

    SET_Journey(state, value) {
      state.sectionJourney = value
    },

    SET_Subjects(state, value) {
      state.sectionSubjects = value
    },

    SET_Feedback(state, value) {
      state.sectionFeedback = value
    },
    
    SET_Achievements(state, value) {
      state.sectionAchievements = value
    },

    SET_Book(state, value) {
      state.sectionBook = value
    },

    SET_Whatsapp(state, value) {
      state.sectionWhatsapp = value
    },
    
    SET_SECTION(state, value) {
      state.sections = value
    },
  },

  actions: {
    async sectionBanner({commit}, value){
      const params = new URLSearchParams();
      params.append("name[]", value);
      await axios.get(`/sections`, { params })
          .then(res => commit("SET_Banner",res.data.data))
    },

    async sectionWork({commit}, value){
      const params = new URLSearchParams();
      params.append("name[]", value);
      await axios.get(`/sections`, { params })
          .then(res => commit("SET_Work",res.data.data))
    },

    async sectionBuild({commit}, value){
      const params = new URLSearchParams();
      params.append("name[]", value);
      await axios.get(`/sections`, { params })
          .then(res => commit("SET_Build",res.data.data))
    },

    async sectionJourney({commit}, value){
      const params = new URLSearchParams();
      params.append("name[]", value);
      await axios.get(`/sections`, { params })
          .then(res => commit("SET_Journey",res.data.data))
    },

    async sectionSubjects({commit}, value){
      const params = new URLSearchParams();
      params.append("name[]", value);
      await axios.get(`/sections`, { params })
          .then(res => commit("SET_Subjects",res.data.data))
    },

    async sectionFeedback({commit}, value){
      const params = new URLSearchParams();
      params.append("name[]", value);
      await axios.get(`/sections`, { params })
          .then(res => commit("SET_Feedback",res.data.data))
    },

    async sectionAchievements({commit}, value){
      const params = new URLSearchParams();
      params.append("name[]", value);
      await axios.get(`/sections`, { params })
          .then(res => commit("SET_Achievements",res.data.data))
    },

    async sectionBook({commit}, value){
      const params = new URLSearchParams();
      params.append("name[]", value);
      await axios.get(`/sections`, { params })
          .then(res => commit("SET_Book",res.data.data))
    },

    async sectionWhatsapp({commit}, value){
      const params = new URLSearchParams();
      params.append("name[]", value);
      await axios.get(`/sections`, { params })
          .then(res => commit("SET_Whatsapp",res.data.data))
    },

    async section({commit}){
      await axios.get(`/sections`)
          .then(res => commit("SET_SECTION",res.data.data))
    },    
  }
}
