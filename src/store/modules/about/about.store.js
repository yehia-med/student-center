// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    abouts: null,
  },

  getters: {
    getabouts: (state) => state.abouts
  },

  mutations: {
    SET_ABOUT(state, value){
      state.abouts = value
    }
  },

  actions: {
    abouts({commit}){
      axios.get(`/about-us`)
          .then(res => commit("SET_ABOUT",res.data.data))
    },
  }
}
