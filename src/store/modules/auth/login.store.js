// router
import router from "@/router";

const state = {
   token: null,
};

// actions
const actions = {
  // attempt to login
  async attemptLogin({ commit }, response) {
    if (response) {
      localStorage.setItem('token', response.data.data.token);
      localStorage.setItem('verified', response.data.data.user.verified);
      localStorage.setItem('role', response.data.data.role);
      commit("setToken", response.data.data.token);
      router.push({ name: "ClientAccountLayout" });
    }
  },
};

// mutations
const mutations = {
  setToken(state, token) {
    state.token = token;
  }
};


export default {
  namespaced: true,
  state,
  actions,
  mutations
};
