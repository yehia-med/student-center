// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    myAccommodations: null,
    loadingDataWaiting: false,
  },

  getters: {
    getMyAccommodations: (state) => state.myAccommodations,
    // loading
    getLoadingDataWaiting: (state) => state.loadingDataWaiting
  },

  mutations: {
    MY_ACCOMMODATIONS(state, value){
      state.myAccommodations = value
    },
    LOADING_DATA_WAITING(state, value){
      state.loadingDataWaiting = value
    },
  },

  actions: {
    myAccommodations({commit}, page){
      const params = new URLSearchParams();
      params.append("per_page", 8);
      page && params.append("page", page);
      axios.get(`user/accommodations`, {params, headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}})
          .then(res => {
            commit("MY_ACCOMMODATIONS", res.data)
            commit("LOADING_DATA_WAITING", false)
          })
          .catch(() => commit("LOADING_DATA_WAITING", false))
    },

    loadingDataWaiting({commit}, value){
      commit("LOADING_DATA_WAITING", value)
    },
  }
}
