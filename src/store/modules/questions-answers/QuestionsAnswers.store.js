// axios
import axios from "axios";

export default {
  namespaced: true,

  state: {
    questions: null,
    loadingDataWaiting: false,
    searchValue: null,
    loadingQA: false
  },

  getters: {
    getQuestions: (state) => state.questions,
    getLoadingDataWaiting: (state) => state.loadingDataWaiting,
    loadingQA: (state) => state.loadingQA
  },

  mutations: {
    QUESTIONS(state, value){
      state.questions = value
    },
    LOADING_DATA_WAITING(state, value){
      state.loadingDataWaiting = value
    },
    SEARCH_VALUE(state, value){
      state.searchValue = value
    },
    LOADING_QA(state, value){
      state.loadingQA = value
    },
  },

  actions: {
    questions({commit, state}, page){
      const params = new URLSearchParams();
      params.append("per_page", 5);
      page && params.append("page", page);
      state.searchValue && params.append("search", state.searchValue);
      axios.get(`questions`, { params })
          .then(res => {
            commit("QUESTIONS", res.data)
            commit("LOADING_DATA_WAITING", false)
          })
          .catch(() => commit("LOADING_DATA_WAITING", false))
    },

    loadingDataWaiting({commit}, value){
      commit("LOADING_DATA_WAITING", value)
    },

    searchValue({commit}, value){
      commit("SEARCH_VALUE", value)
    },

    upvote({commit}, value){
      const data = new FormData();
      data.append("vote", "1");
      axios.post(`questions/${value}/vote`, data, {
        headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}
      })
      .then(res => {
        this.questions();
        commit("LOADING_QA", res.data.message)
        commit("LOADING_DATA_WAITING", false)
      })
      .catch(err => {
          commit("LOADING_QA", err.response.data.message)
          commit("LOADING_DATA_WAITING", true)
      })
    },

    downvote({commit}, value){
      const data = new FormData();
      data.append("vote", "-1");
      axios.post(`questions/${value}/vote`, data, {
        headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}
      })
      .then(res => {
        commit("LOADING_QA", res.data.message)
        commit("LOADING_DATA_WAITING", false)
      })
      .catch(err => {
          commit("LOADING_QA", err.response.data.message)
          commit("LOADING_DATA_WAITING", true)
      })
    },

    add_questions({commit}, value){
      const data = new FormData();
      data.append("question", value.question);
      axios.post(`questions`, data, {
        headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}
      })
      .then(res => {
        commit("LOADING_QA", res.data.message)
        commit("LOADING_DATA_WAITING", false)
      })
      .catch(err => {
          commit("LOADING_QA", err.response.data.message)
          commit("LOADING_DATA_WAITING", true)
      })
    },


    edit_questions({commit}, value){
      const data = new URLSearchParams();
      data.append("question", value.question);
      axios.put(`questions/${value.post_id}`, data, {
        headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}
      })
      .then(res => {
        commit("LOADING_QA", res.data.message)
        commit("LOADING_DATA_WAITING", false)
      })
      .catch(err => {
          commit("LOADING_QA", err.message)
          commit("LOADING_DATA_WAITING", true)
      })
    },


    delete_questions({commit}, value){
      axios.delete(`questions/${value}`, {
        headers: { Authorization: `Bearer ${localStorage.getItem('token')}` },
      })
      .then(res => {
        commit("LOADING_QA", res.data.message)
        commit("LOADING_DATA_WAITING", false)
      })
      .catch(err => {
          commit("LOADING_QA", err.response.data.message)
          commit("LOADING_DATA_WAITING", true)
      })
    },

    add_answer({commit}, value){
      const data = new FormData();
      data.append("answer", value.question);
      axios.post(`questions/${value.q_id}/answers`, data, {
        headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}
      })
      .then(res => {
        commit("LOADING_QA", res.data.message)
        commit("LOADING_DATA_WAITING", false)
      })
      .catch(err => {
          commit("LOADING_QA", err.response.data.message)
          commit("LOADING_DATA_WAITING", true)
      })
    },

  }
}
