// axios
import axios from "axios";
// router
import { useToast } from "vue-toastification";

export default {
  namespaced: true,

  setup: () => ({ 
    toast: useToast(),
  }),


  state: {
    CreatePost: null,
    statusPost: null,
    loadingDataWaiting: false,
    countriesKeys: [],
    dropdownsOptions: {
      cities: null,
    },
  },

  getters: {
    getCreatePost: (state) => state.CreatePost,
    getStatusPost: (state) => state.statusPost,
    getLoadingDataWaiting: (state) => state.loadingDataWaiting,
    getCountriesKeys: (state) => state.countriesKeys,
    getDropdownsCitiesOptions: (state) => state.dropdownsOptions.cities
  },

  mutations: {
    CREATE_POST(state, value){
      state.CreatePost = value
    },
    STATUS_POST(state, value){
      state.statusPost = value
    },
    LOADING_DATA_WAITING(state, value){
      state.loadingDataWaiting = value
    },
    DROPDOWNS_OPTIONS_CITIES(state, value){
      state.dropdownsOptions.cities = value
    },
    COUNTRIES_KEYS(state, value){
      state.countriesKeys = value
    },
  },

  actions: {
    // Create post
    CreatePost({commit}, post_data){
      const data = new FormData();

      post_data.name && data.append("name", post_data.name);
      post_data.email && data.append("email", post_data.email);
      post_data.phone && data.append("phone", post_data.phone);
      post_data.phone_code.code && data.append("phone_code", post_data.phone_code.code);
      post_data.whatsapp && data.append("whatsapp", post_data.whatsapp);
      post_data.whatsapp_code.code && data.append("whatsapp_code", post_data.whatsapp_code.code);
      post_data.title && data.append("title", post_data.title);
      post_data.type?.id && data.append("type", post_data.type.id);
      post_data.num_rooms && data.append("num_rooms", post_data.num_rooms);
      post_data.price && data.append("price", post_data.price);
      post_data.currency_id?.id && data.append("currency_id", post_data.currency_id.id);
      post_data.country_id?.id && data.append("country_id", post_data.country_id.id);
      post_data.city_id?.id && data.append("city_id", post_data.city_id.id);
      post_data.lat && data?.append("lat", post_data.lat);
      post_data.lng && data?.append("lng", post_data.lng);
      post_data.description && data.append("description", post_data.description);

      const length = post_data.photos.length;
      for (let i = 0; i < length; i++) {
        data.append("images[]", post_data.photos[i]);
      }

      axios.post(`${post_data.post_type}`, data, {
        headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}
      })
      .then(res => {
        commit("CREATE_POST", res.data.message)
        commit("STATUS_POST", true)
        commit("LOADING_DATA_WAITING", false)
      })
      .catch(err => {
          commit("CREATE_POST", err.response.data.message)
          commit("STATUS_POST", false)
          commit("LOADING_DATA_WAITING", true)
      })
    },

    // loading Data Waiting
    loadingDataWaiting({commit}, value){
      commit("LOADING_DATA_WAITING", value)
    },

    // Countries Keys
    countriesKeys({commit}){
      axios.get(`countries`)
      .then(res => commit("COUNTRIES_KEYS", res.data.data))
    },

    // Cities Options by countery id
    dropdownCitiesOptions({commit}, countryId){
      axios.get(`/countries/${countryId}/cities`)
      .then(res => commit("DROPDOWNS_OPTIONS_CITIES", res.data.data))
      .catch(err => console.log(err.message))
    },
  }
}
