// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    blogs: null,
  },

  getters: {
    getblogs: (state) => state.blogs
  },

  mutations: {
    SET_BLOGS(state, value){
      state.blogs = value
    }
  },

  actions: {
    blogs({commit}){
      axios.get(`/news`)
          .then(res => commit("SET_BLOGS",res.data.data))
    },

    blogsearch({commit}, value){
      const params = new URLSearchParams();
      params.append("search", value);
      axios.get(`/news`, { params })
          .then(res => commit("SET_BLOGS",res.data))
    },
  }
}
