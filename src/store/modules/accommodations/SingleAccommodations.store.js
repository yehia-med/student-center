// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    SingleAccommodations: null,
    loadingDataWaiting: false
  },

  getters: {
    getSingleAccommodations: (state) => state.SingleAccommodations,
    // loading
    getLoadingDataWaiting: (state) => state.loadingDataWaiting
  },

  mutations: {
    MY_ACCOMMODATIONS(state, value) {
      state.SingleAccommodations = value;
    },
    LOADING_DATA_WAITING(state, value) {
      state.loadingDataWaiting = value;
    }
  },

  actions: {
    singleAccommodations({ commit }, post_id) {
      axios
        .get(`accommodations/${post_id}`)
        .then((res) => {
          commit("MY_ACCOMMODATIONS", res.data);
          commit("LOADING_DATA_WAITING", false);
        })
        .catch(() => commit("LOADING_DATA_WAITING", false));
    },

    loadingDataWaiting({ commit }, value) {
      commit("LOADING_DATA_WAITING", value);
    }
  }
};
