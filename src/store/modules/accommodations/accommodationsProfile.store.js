// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    accommodationData: null,
    accommodationLocation: null,
    relatedAccommodations: null
 },

  getters: {
    getAccommodationData: (state) => state.accommodationData,
    getAccommodationName: (state) => state.accommodationData?.name,
    getAccommodationInstitute: (state) => state.accommodationData?.institute,
    getAccommodationLocation: (state) => state.accommodationLocation,
    getAccommodationRequirements: (state) => state.accommodationData?.requirements,
    getAccommodationDescription: (state) => state.accommodationData?.description,
    getAccommodationPrice: (state) => state.accommodationData?.price,
    getAccommodationDates: (state) => state.accommodationData?.dates,
    getRelatedAccommodations: (state) => state.relatedAccommodations
  },

  mutations: {
    ACCOMMODATION_DATA(state, value){
      state.accommodationData = value
    },
    ACCOMMODATION_LOCATION(state, {street_name, street_number, zip_code, city, country, lat, lng}){
      state.accommodationLocation = {street_name, street_number, zip_code, city, country, lat, lng}
    },
    RELATED_ACCOMMODATIONS(state, value){
      state.relatedAccommodations = value
    },
  },

  actions: {
    courseData({commit, state}, accommodationId){
      axios.get(`accommodations/${accommodationId}`)
        .then(res => {
          commit("ACCOMMODATION_DATA", res.data.data)
          commit("ACCOMMODATION_LOCATION", res.data.data)
        })
        // .catch(() => commit("LOADING_DATA_WAITING", false))
      if (state.getAccommodationInstitute) {
        let params = new URLSearchParams();
        params.append("per_page", 3);
        params.append("institute_id", state.getAccommodationInstitute.id)
        axios.get(`accommodations`, {params})
        .then(res => {
          commit("RELATED_ACCOMMODATIONS", res.data.data)
        })   
      } 
    },
  }
}
