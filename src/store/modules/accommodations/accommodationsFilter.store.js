// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    dropdownsOptions: {
      countries: null,
      cities: null,
    },
  },

  getters: {
    getDropdownsCountriesOptions: (state) => state.dropdownsOptions.countries,
    getDropdownsCitiesOptions: (state) => state.dropdownsOptions.cities,
  },

  mutations: {
    DROPDOWNS_OPTIONS_COUNTRIES(state, value){
      state.dropdownsOptions.countries = value
    },
    DROPDOWNS_OPTIONS_CITIES(state, value){
      state.dropdownsOptions.cities = value
    },
  },

  actions: {
    dropdownsOptions({commit}){
      axios.get(`countries`)
      .then((res) => commit("DROPDOWNS_OPTIONS_COUNTRIES", res.data.data))
    },
    
    dropdownCitiesOptions({commit}, countryId){
      axios.get(`/countries/${countryId}/cities`)
      .then(res => commit("DROPDOWNS_OPTIONS_CITIES", res.data.data))
      .catch(err => console.log(err.message))
    }
  },
}
