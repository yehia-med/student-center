// axios
import axios from "axios";

export default {
  namespaced: true,

  state: {
    accommodations: null,
    loadingDataWaiting: false,
    searchValue: null,
    filterValues: {
      country: null,
      city: null,
      num_rooms: 0,
      type: null,
      priceRange: { min_price: null, max_price: null },
      sortBy: null,
    }
  },

  getters: {
    getAccommodations: (state) => state.accommodations,
    // loading
    getLoadingDataWaiting: (state) => state.loadingDataWaiting
  },

  mutations: {
    ACCOMMODATIONS(state, value){
      state.accommodations = value
    },
    LOADING_DATA_WAITING(state, value){
      state.loadingDataWaiting = value
    },
    SEARCH_VALUE(state, value){
      state.searchValue = value
    },
    FILTER_VALUES(state, value){
      state.filterValues = value
    },
  },

  actions: {
    accommodations({commit, state}, page){
      const params = new URLSearchParams();
      params.append("per_page", 10);
      page && params.append("page", page);
      state.searchValue && params.append("search", state.searchValue);
      state.filterValues.country && params.append("country_id", state.filterValues.country);
      state.filterValues.city && params.append("city_id", state.filterValues.city);
      state.filterValues.num_rooms && params.append("num_rooms", state.filterValues.num_rooms);
      state.filterValues.type && params.append("type", state.filterValues.type);
      state.filterValues.priceRange.min_price && params.append("min_price", state.filterValues.priceRange.min_price);
      state.filterValues.priceRange.max_price && params.append("max_price", state.filterValues.priceRange.max_price);
      state.filterValues.sortBy && params.append("sort[price]", state.filterValues.sortBy === 1 ? 'desc': 'asc');
      axios.get(`accommodations`, { params })
          .then(res => {
            commit("ACCOMMODATIONS", res.data)
            commit("LOADING_DATA_WAITING", false)
          })
          .catch(() => commit("LOADING_DATA_WAITING", false))
    },

    loadingDataWaiting({commit}, value){
      commit("LOADING_DATA_WAITING", value)
    },

    searchValue({commit}, value){
      commit("SEARCH_VALUE", value)
    },

    filterValues({commit}, value){
      commit("FILTER_VALUES", value)
    }
  }
}
