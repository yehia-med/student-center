// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    sales: null,
    loadingDataWaiting: false,
    searchValue: null,
    filterValues: {
      country: null,
      city: null,
    }
  },

  getters: {
    getSales: (state) => state.sales,
    // loading
    getLoadingDataWaiting: (state) => state.loadingDataWaiting
  },

  mutations: {
    SALES(state, value){
      state.sales = value
    },
    LOADING_DATA_WAITING(state, value){
      state.loadingDataWaiting = value
    },
    SEARCH_VALUE(state, value){
      state.searchValue = value
    },
    FILTER_VALUES(state, value){
      state.filterValues = value
    },
  },

  actions: {
    sales({commit, state}, page){
      const params = new URLSearchParams();
      params.append("per_page", 10);
      page && params.append("page", page);
      state.searchValue && params.append("search", state.searchValue);
      state.filterValues.country && params.append("country_id", state.filterValues.country);
      state.filterValues.city && params.append("city_id", state.filterValues.city);
      axios.get(`sales`, { params })
          .then(res => {
            commit("SALES", res.data)
            commit("LOADING_DATA_WAITING", false)
          })
          .catch(() => commit("LOADING_DATA_WAITING", false))
    },

    loadingDataWaiting({commit}, value){
      commit("LOADING_DATA_WAITING", value)
    },

    searchValue({commit}, value){
      console.log(value)
      commit("SEARCH_VALUE", value)
    },

    filterValues({commit}, value){
      commit("FILTER_VALUES", value)
    }
  }
}
