// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    Sell_Buy: null,
    loadingDataWaiting: false,
  },

  getters: {
    getSellBuy: (state) => state.Sell_Buy,
    // loading
    getLoadingDataWaiting: (state) => state.loadingDataWaiting
  },

  mutations: {
    MY_SELL_BUY(state, value){
      state.Sell_Buy = value
    },
    LOADING_DATA_WAITING(state, value){
      state.loadingDataWaiting = value
    },
  },

  actions: {
    SellBuyProfile({commit}, post_id){
      axios.get(`sales/${post_id}`)
        .then(res => {
          commit("MY_SELL_BUY", res.data)
          commit("LOADING_DATA_WAITING", false)
        })
        .catch(() => commit("LOADING_DATA_WAITING", false))
    },

    loadingDataWaiting({commit}, value){
      commit("LOADING_DATA_WAITING", value)
    },
  }
}
