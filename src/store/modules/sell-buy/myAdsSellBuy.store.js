// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    mySellAndBuy: null,
    loadingDataWaiting: false,
  },

  getters: {
    getMySellAndBuy: (state) => state.mySellAndBuy,
    // loading
    getLoadingDataWaiting: (state) => state.loadingDataWaiting
  },

  mutations: {
    MY_SELL_AND_BUY(state, value){
      state.mySellAndBuy = value
    },
    LOADING_DATA_WAITING(state, value){
      state.loadingDataWaiting = value
    },
  },

  actions: {
    mySellAndBuy({commit}, page){
      const params = new URLSearchParams();
      params.append("per_page", 8);
      page && params.append("page", page);
      axios.get(`user/sales`, {params, headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}})
          .then(res => {
            commit("MY_SELL_AND_BUY", res.data)
            commit("LOADING_DATA_WAITING", false)
          })
          .catch(() => commit("LOADING_DATA_WAITING", false))
    },

    loadingDataWaiting({commit}, value){
      commit("LOADING_DATA_WAITING", value)
    },
  }
}
