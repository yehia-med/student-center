// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    englishCourses: null,
    loadingDataWaiting: false,
    searchValue: null,
    filterValues: {
      priceRange: { min_price: null, max_price: null },
      country: null,
      city: null,
      age: null, 
      school: null,
      sortBy: null,
    }
  },

  getters: {
    getEnglishCourses: (state) => state.englishCourses,
    // loading
    getLoadingDataWaiting: (state) => state.loadingDataWaiting
  },

  mutations: {
    ENGLISH_COURSES(state, value){
      state.englishCourses = value
    },
    LOADING_DATA_WAITING(state, value){
      state.loadingDataWaiting = value
    },
    SEARCH_VALUE(state, value){
      state.searchValue = value
    },
    FILTER_VALUES(state, value){
      state.filterValues = value
    },
  },

  actions: {
    englishCourses({commit, state}, page){
      const params = new URLSearchParams();
      params.append("per_page", 9);
      page && params.append("page", page);
      state.searchValue && params.append("search", state.searchValue);
      state.filterValues.priceRange.min_price && params.append("min_price", state.filterValues.priceRange.min_price);
      state.filterValues.priceRange.max_price && params.append("max_price", state.filterValues.priceRange.max_price);
      state.filterValues.country && params.append("country_id", state.filterValues.country);
      state.filterValues.city && params.append("city_id", state.filterValues.city);
      state.filterValues.age && params.append("ageGroup_id", state.filterValues.age);
      state.filterValues.school && params.append("institute_id", state.filterValues.school);
      state.filterValues.sortBy && params.append("sort[price]", state.filterValues.sortBy === 1 ? 'desc': 'asc');
      axios.get(`english-courses`, { params })
          .then(res => {
            commit("ENGLISH_COURSES", res.data)
            commit("LOADING_DATA_WAITING", false)
          })
          .catch(() => commit("LOADING_DATA_WAITING", false))
    },

    loadingDataWaiting({commit}, value){
      commit("LOADING_DATA_WAITING", value)
    },

    searchValue({commit}, value){
      commit("SEARCH_VALUE", value)
    },

    filterValues({commit}, value){
      commit("FILTER_VALUES", value)
    }
  }
}
