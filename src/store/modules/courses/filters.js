// axios
import axios, { all } from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    dropdownsOptions: {
      countries: null,
      cities: null,
    },
    unDropdownsOptions: {
      levels: null,
      degrees: null,
      subjects: null,
      startDate: null
    },
    enDropdownsOptions: {
      age: null,
      institutes: null,
      sortBy: null,
    },
  },

  getters: {
    getDropdownsCountriesOptions: (state) => state.dropdownsOptions.countries,
    getDropdownsCitiesOptions: (state) => state.dropdownsOptions.cities,
    // University Courses
    getDropdownsLevelsOptions: (state) => state.unDropdownsOptions.levels,
    getDropdownsDegreesOptions: (state) => state.unDropdownsOptions.degrees,
    getDropdownsSubjectsOptions: (state) => state.unDropdownsOptions.subjects,
    getDropdownsStartDateOptions: (state) => state.unDropdownsOptions.startDate,
    // English Courses
    getDropdownsAgeOptions: (state) => state.enDropdownsOptions.age,
    getDropdownsInstitutesOptions: (state) => state.enDropdownsOptions.institutes,
    getDropdownsSortByOptions: (state) => state.enDropdownsOptions.sortBy,
  },

  mutations: {
    DROPDOWNS_OPTIONS_COUNTRIES(state, value){
      state.dropdownsOptions.countries = value
    },
    DROPDOWNS_OPTIONS_CITIES(state, value){
      state.dropdownsOptions.cities = value
    },
    // University Courses
    DROPDOWNS_OPTIONS_LEVELS(state, value){
      state.unDropdownsOptions.levels = value
    },
    DROPDOWNS_OPTIONS_DEGREES(state, value){
      state.unDropdownsOptions.degrees = value
    },
    DROPDOWNS_OPTIONS_SUBJECTS(state, value){
      state.unDropdownsOptions.subjects = value
    },
    // English Courses
    DROPDOWNS_OPTIONS_AGE(state, value){
      state.enDropdownsOptions.age = value
    },
    DROPDOWNS_OPTIONS_INSTITUTES(state, value){
      state.enDropdownsOptions.institutes = value
    },
  },

  actions: {
    dropdownsOptions({commit}, coursesType){
      if(coursesType === "universityCourses"){
        axios.all([`/countries`,`/subjects`,`/levels`,`/degree-type`].map((url => axios.get(url))))
        .then(axios.spread((countries, subjects, levels, degrees) => {
          commit("DROPDOWNS_OPTIONS_COUNTRIES", countries.data.data),
          commit("DROPDOWNS_OPTIONS_SUBJECTS", subjects.data.data)
          commit("DROPDOWNS_OPTIONS_LEVELS", levels.data.data)
          commit("DROPDOWNS_OPTIONS_DEGREES", degrees.data.data)
        }))
      } else if(coursesType === "englishCourses"){
        axios.all([`/countries`,`/age-groups`,`/institutes`].map((url => axios.get(url))))
        .then(axios.spread((countries, age, institutes) => {
          commit("DROPDOWNS_OPTIONS_COUNTRIES", countries.data.data),
          commit("DROPDOWNS_OPTIONS_AGE", age.data.data)
          commit("DROPDOWNS_OPTIONS_INSTITUTES", institutes.data.data)
        }))
      }
    },

    dropdownCitiesOptions({commit}, countryId){
      axios.get(`/countries/${countryId}/cities`)
      .then(res => commit("DROPDOWNS_OPTIONS_CITIES", res.data.data))
      .catch(err => console.log(err.message))
    },
  }
}
