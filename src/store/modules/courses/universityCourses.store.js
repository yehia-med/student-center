// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    universityCourses: null,
    loadingDataWaiting: false,
    searchValue: null,
    filterValues: {
      country: null,
      city: null,
      level: null, 
      degree: null,
      subject: null,
      startDate: null
    }
  },

  getters: {
    getUniversityCourses: (state) => state.universityCourses,
    // loading
    getLoadingDataWaiting: (state) => state.loadingDataWaiting
  },

  mutations: {
    UNIVERSITY_COURSES(state, value){
      state.universityCourses = value
    },
    LOADING_DATA_WAITING(state, value){
      state.loadingDataWaiting = value
    },
    SEARCH_VALUE(state, value){
      state.searchValue = value
    },
    FILTER_VALUES(state, value){
      state.filterValues = value
    },
  },

  actions: {
    universityCourses({commit, state}, page){
      const params = new URLSearchParams();
      params.append("per_page", 9);
      page && params.append("page", page);
      state.searchValue && params.append("search", state.searchValue);
      state.filterValues.country && params.append("country_id", state.filterValues.country);
      state.filterValues.city && params.append("city_id", state.filterValues.city);
      state.filterValues.level && params.append("level_id", state.filterValues.level);
      state.filterValues.degree && params.append("degreeType_id", state.filterValues.degree);
      state.filterValues.subject && params.append("subject_id", state.filterValues.subject);
      state.filterValues.startDate && params.append("start_date", state.filterValues.startDate);
      axios.get(`university-courses`, { params })
          .then(res => {
            commit("UNIVERSITY_COURSES", res.data)
            commit("LOADING_DATA_WAITING", false)
          })
          .catch(() => commit("LOADING_DATA_WAITING", false))
    },

    loadingDataWaiting({commit}, value){
      commit("LOADING_DATA_WAITING", value)
    },

    searchValue({commit}, value){
      commit("SEARCH_VALUE", value)
    },

    filterValues({commit}, value){
      commit("FILTER_VALUES", value)
    }
  }
}
