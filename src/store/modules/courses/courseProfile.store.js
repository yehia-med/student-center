// axios
import axios from "axios";
// router
// import router from "@/router";
// toast
import { useToast } from "vue-toastification";
const toast = useToast();

export default {
  namespaced: true,

  state: {
    courseData: null,
    courseDataLocation: null,
    relatedCourses: null,
    dialogAfterRes: true,
    btnLoading: false,
    selectLoading: false,
    calcPrice: null,
    accommodationsList: null,
    universitySuccessMsg: false,
    toggleSignIn: false
  },

  getters: {
    getCourseData: (state) => state.courseData,
    getCourseName: (state) => state.courseData?.name,
    getCourseInstitute: (state) => state.courseData?.institute,
    getCourseUniversity: (state) => state.courseData?.university,
    getCourseImages: (state) => state.courseData?.image,
    getCourseDataLocation: (state) => state.courseDataLocation,
    getCourseRequirements: (state) => state.courseData?.requirements,
    getCourseDescription: (state) => state.courseData?.description,
    getCourseHasSale: (state) => state.courseData?.has_sale,
    getCourseSelfPrice: (state) => state.courseData?.self_price,
    getCourseSelfSalePrice: (state) => state.courseData?.self_price_sale,
    getCoursePricePer: (state) => state.courseData?.price_per,
    getCourseDates: (state) => state.courseData?.dates,
    getRelatedCourses: (state) => state.relatedCourses,
    getDialogAfterRes: (state) => state.dialogAfterRes,
    getBtnLoading: (state) => state.btnLoading,
    getSelectLoading: (state) => state.selectLoading,
    getCalcPrice: (state) => state.calcPrice,
    getAccommodationsList: (state) => state.accommodationsList,
    getUniversitySuccessMsg: (state) => state.universitySuccessMsg,
    getToggleSignIn: (state) => state.toggleSignIn
  },

  mutations: {
    COURSE_DATA(state, value) {
      state.courseData = value;
    },
    COURSE_DATA_LOCATION(state, { street_name, street_number, zip_code, city, country, lat, lng }) {
      state.courseDataLocation = { street_name, street_number, zip_code, city, country, lat, lng };
    },
    RELATED_COURSES(state, value) {
      state.relatedCourses = value;
    },
    RESET_RELATED_COURSES(state) {
      state.relatedCourses = null;
    },
    DIALOG_AFTER_RES(state, value) {
      state.dialogAfterRes = value;
    },
    BTN_LOADING(state, value) {
      state.btnLoading = value;
    },
    SELECT_LOADING(state, value) {
      state.selectLoading = value;
    },
    CALCULATE_PRICE(state, value) {
      state.calcPrice = value;
    },
    ACCOMMODATIONS(state, value) {
      state.accommodationsList = value;
    },
    UNIVERSITY_SUCCESS_MSG(state, status) {
      state.universitySuccessMsg = status;
    },
    TOGGLE_SIGN_IN(state) {
      state.toggleSignIn = !state.toggleSignIn;
    }
  },

  actions: {
    courseData({ commit }, [courseType, courseId]) {
      axios
        .get(
          `${
            courseType === "UniversityCourseProfile" ? "university" : "english"
          }-courses/${courseId}`
        )
        .then((res) => {
          commit("COURSE_DATA", res.data.data);
          commit("COURSE_DATA_LOCATION", res.data.data);

          let params = new URLSearchParams();
          params.append("per_page", 3);
          res.data.data.institute && params.append("institute_id", res.data.data.institute.id);
          res.data.data.university && params.append("university_id", res.data.data.university.id);
          axios
            .get(`${courseType === "UniversityCourseProfile" ? "university" : "english"}-courses`, {
              params
            })
            .then((res) => {
              commit("RELATED_COURSES", res.data.data);
            });
        })
        .catch(() => commit("LOADING_DATA_WAITING", false));
    },

    resetRelatedCourses({ commit }) {
      commit("RESET_RELATED_COURSES");
    },

    submitEditsDataCourse({ commit }, [courseType, course]) {
      commit("BTN_LOADING", true);
      let data = new FormData();
      course.documents?.length > 0 && data.append("documents[]", ...course.documents);
      course.type && data.append("type", course.type);
      course.fund_type && data.append("fund_type", course.fund_type);
      course.start_date.code && data.append("start_date", course.start_date.code);
      course.transportation || course.transportation === 0
        ? data.append("has_transportation", course.transportation)
        : null;
      course.accommodation || course.accommodation === 0
        ? data.append("has_accommodation", course.accommodation)
        : null;
      course.accommodation && data.append("accommodation_id", course.accommodation_id);
      let url = String;
      courseType === "UniversityCourseProfile"
        ? (url = `user/university-courses/${course.id}/apply`)
        : (url = `user/applications/apply-english/${course.id}`);
      localStorage.getItem("token")
        ? axios
            .post(url, data, {
              headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
            })
            .then((res) => {
              commit("BTN_LOADING", false);
              commit("DIALOG_AFTER_RES", false);
              courseType === "UniversityCourseProfile"
                ? (commit("UNIVERSITY_SUCCESS_MSG", true), toast.success(res.data.message))
                : (toast.success(res.data.message || "Applied Successfully"),
                  (window.location.href = res.data.data.payment_url));
            })
            .catch((err) => {
              commit("BTN_LOADING", false);
              toast.error(err.response.data.message);
            })
        : (commit("DIALOG_AFTER_RES", false),
          commit("BTN_LOADING", false),
          toast.error("For applying, please Sign in first"),
          commit("TOGGLE_SIGN_IN"));
    },

    calculatePrice({ commit }, [courseId, course]) {
      commit("SELECT_LOADING", true);
      let data = new FormData();
      course.fund_type !== null && data.append("fund_type", course.fund_type);
      course.transportation !== null && data.append("has_transportation", course.transportation);
      course.accommodation !== null && data.append("has_accommodation", course.accommodation);
      course.accommodation && data.append("accommodation_id", course.accommodation_id);
      data.append("type", "english");
      axios
        .post(`user/applications/calculate-price/${courseId}`, data, {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then((res) => {
          commit("CALCULATE_PRICE", res.data);
          commit("SELECT_LOADING", false);
        });
    },

    Accommodations({ commit }, courseId) {
      localStorage.getItem("token")
        ? axios
            .get(`user/applications/accommodations/${courseId}`, {
              headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
            })
            .then((res) => commit("ACCOMMODATIONS", res.data.data))
        : toast.error("For applying, please Sign in first");
    },

    uniSuccessMessage({ commit }) {
      commit("UNIVERSITY_SUCCESS_MSG", false);
    },

    resetDialog({ commit }) {
      commit("DIALOG_AFTER_RES", true);
    }
  }
};
