// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    terms: null,
    privacy: null
  },

  getters: {
    getterms: (state) => state.terms,
    getprivacy: (state) => state.privacy
  },

  mutations: {
    SET_TERMS(state, value){
      state.terms = value
    },
    SET_PRIVACY(state, value) {
      state.privacy = value
    }
  },

  actions: {
    terms({commit}){
      axios.get(`/terms`)
          .then(res => commit("SET_TERMS",res.data.data))
    },
    privacy({commit}){
      axios.get(`/privacy`)
          .then(res => commit("SET_PRIVACY",res.data.data))
    },
  }
}
