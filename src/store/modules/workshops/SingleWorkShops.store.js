// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    work_shops: null,
    loadingDataWaiting: false,
  },

  getters: {
    getWorkShops: (state) => state.work_shops,
    // loading
    getLoadingDataWaiting: (state) => state.loadingDataWaiting
  },

  mutations: {
    MY_WoRK_SHOPS(state, value){
      state.work_shops = value
    },
    LOADING_DATA_WAITING(state, value){
      state.loadingDataWaiting = value
    },
  },

  actions: {
    WorkShopsProfile({commit}, post_id){
      axios.get(`workshops/${post_id}`)
      .then(res => {
        commit("MY_WoRK_SHOPS", res.data)
        commit("LOADING_DATA_WAITING", false)
      })
      .catch(() => commit("LOADING_DATA_WAITING", false))
    },

    loadingDataWaiting({commit}, value){
      commit("LOADING_DATA_WAITING", value)
    },
  }
}
