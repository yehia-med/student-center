// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    workshops: null,
    loadingDataWaiting: false,
    searchValue: null,
    filterValues: {
      country: null,
      city: null,
      type: null,
      start_date: null,
      end_date: null
    }
  },

  getters: {
    getWorkshops: (state) => state.workshops,
    // loading
    getLoadingDataWaiting: (state) => state.loadingDataWaiting
  },

  mutations: {
    WORKSHOPS(state, value){
      state.workshops = value
    },
    LOADING_DATA_WAITING(state, value){
      state.loadingDataWaiting = value
    },
    SEARCH_VALUE(state, value){
      state.searchValue = value
    },
    FILTER_VALUES(state, value){
      state.filterValues = value
    },
  },

  actions: {
    workshops({commit, state}, page){
      const params = new URLSearchParams();
      params.append("per_page", 10);
      page && params.append("page", page);
      state.searchValue && params.append("search", state.searchValue);
      state.filterValues.country && params.append("country_id", state.filterValues.country);
      state.filterValues.city && params.append("city_id", state.filterValues.city);
      state.filterValues.type && params.append("type", state.filterValues.type);
      state.filterValues.start_date && params.append("start_date", state.filterValues.start_date);
      state.filterValues.end_date && params.append("end_date", state.filterValues.end_date);
      axios.get(`workshops`, { params })
          .then(res => {
            commit("WORKSHOPS", res.data)
            commit("LOADING_DATA_WAITING", false)
          })
          .catch(() => commit("LOADING_DATA_WAITING", false))
    },

    loadingDataWaiting({commit}, value){
      commit("LOADING_DATA_WAITING", value)
    },

    searchValue({commit}, value){
      commit("SEARCH_VALUE", value)
    },

    filterValues({commit}, value){
      commit("FILTER_VALUES", value)
    }
  }
}
