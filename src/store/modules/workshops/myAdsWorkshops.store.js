// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    myWorkshops: null,
    loadingDataWaiting: false,
  },

  getters: {
    getMyWorkshops: (state) => state.myWorkshops,
    // loading
    getLoadingDataWaiting: (state) => state.loadingDataWaiting
  },

  mutations: {
    MY_WORKSHOPS(state, value){
      state.myWorkshops = value
    },
    LOADING_DATA_WAITING(state, value){
      state.loadingDataWaiting = value
    },
  },

  actions: {
    myWorkshops({commit}, page){
      const params = new URLSearchParams();
      params.append("per_page", 8);
      page && params.append("page", page);
      axios.get(`user/workshops`, {params, headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}})
          .then(res => {
            commit("MY_WORKSHOPS", res.data)
            commit("LOADING_DATA_WAITING", false)
          })
          .catch(() => commit("LOADING_DATA_WAITING", false))
    },

    loadingDataWaiting({commit}, value){
      commit("LOADING_DATA_WAITING", value)
    },
  }
}
