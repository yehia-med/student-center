// axios
import axios from "axios";

import { useToast } from "vue-toastification";
const toast = useToast();

export default {
  namespaced: true,

  state: {
    countriesKeys: [],
    profileData: {},
    dialogAfterRes: true,
    btnLoading: false
  },

  getters: {
    getCountriesKeys: (state) => state.countriesKeys,
    getProfileData: (state) => state.profileData,
    getDialogAfterRes: (state) => state.dialogAfterRes,
    getBtnLoading: (state) => state.btnLoading
  },

  mutations: {
    COUNTRIES_KEYS(state, value) {
      state.countriesKeys = value;
    },
    PROFILE_DATA(state, value) {
      state.profileData = value;
    },
    DIALOG_AFTER_RES(state, value) {
      state.dialogAfterRes = value;
    },
    BTN_LOADING(state, value) {
      state.btnLoading = value;
    }
  },

  actions: {
    countriesKeys({ commit }) {
      axios.get(`countries`).then((res) => commit("COUNTRIES_KEYS", res.data.data));
    },

    profileData({ commit }) {
      axios
        .get(`auth/profile`, {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then((res) => commit("PROFILE_DATA", res.data.data.user));
    },

    submitEditsData({ commit }, [userData, photo]) {
      commit("DIALOG_AFTER_RES", true);
      commit("BTN_LOADING", true);
      let data = new FormData();
      photo && data.append("image", photo);
      data.append("_method", "PUT");
      data.append("name", userData.name);
      data.append("country_code", userData.phone_code);
      data.append("phone", userData.phone);
      data.append("email", userData.email);
      data.append("address", userData.address);
      axios
        .post(`auth/profile`, data, {
          headers: { Authorization: `Bearer ${localStorage.getItem("token")}` }
        })
        .then((res) => {
          commit("PROFILE_DATA", res.data.data.user);
          commit("BTN_LOADING", false);
          commit("DIALOG_AFTER_RES", false);
          toast.success(res.data.message);
        })
        .catch((err) => {
          commit("BTN_LOADING", false);
          commit("DIALOG_AFTER_RES", true);
          toast.error(err.response?.data.message);
        });
    }
  }
};
