// axios
import axios from "axios";

import { useToast } from "vue-toastification";
const toast = useToast();

export default {
  namespaced: true,

  state: {
    documents: [],
    loadingDataWaiting: false,
  },

  getters: {
    getDocuments: (state) => state.documents,
    getLoadingDataWaiting: (state) => state.loadingDataWaiting
  },

  mutations: {
    SET_DOCUMENTS(state, value){
      state.documents = value
    },
    LOADING_DATA_WAITING(state, value){
      state.loadingDataWaiting = value
    },
  },

  actions: {
    documents({commit}, page){
      const params = new URLSearchParams();
      params.append("per_page", 10);
      page && params.append("page", page);
      axios.get(`user/documents`,  { params, headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}})
      .then(res => {
        commit("LOADING_DATA_WAITING", false)
        commit("SET_DOCUMENTS", res.data)
      })
      .catch(()=> commit("LOADING_DATA_WAITING", false))
    },
    loadingDataWaiting({commit}, value){
      commit("LOADING_DATA_WAITING", value)
    },
    deleteDocument({dispatch}, documentId){
      axios.delete(`user/documents/${documentId}`, {headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}})
      .then(res => {
        dispatch('documents');
        toast.success(res.data.message);
      })
      .catch(err => toast.error(err?.response?.data.message))
    }
  }
}
