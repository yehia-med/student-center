// axios
import axios from "axios";
// router
// import router from "@/router";

export default {
  namespaced: true,

  state: {
    changePasswordRes: null
  },

  getters: {
    getChangePasswordRes: (state) => state.changePasswordRes
  },

  mutations: {
    CHANGE_PASSWORD_RES(state, value){
      console.log(value)
      state.changePasswordRes = value
    }
  },

  actions: {
    changePassword({commit},[oldPwd, newPwd, confirmPwd]){
      let data = new URLSearchParams();
      data.append("old_password", oldPwd);
      data.append("password", newPwd);
      data.append("password_confirmation", confirmPwd);

      axios.put("auth/change-password", data, {headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`, 'content-type': 'application/x-www-form-urlencoded'}})
      .then(res => commit("CHANGE_PASSWORD_RES", res))
      .catch(err => commit("CHANGE_PASSWORD_RES", err))
    }
  }
}
