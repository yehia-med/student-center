// axios
import axios from "axios";

export default {
  namespaced: true,

  state: {
    applications: [],
    loadingDataWaiting: false,
  },

  getters: {
    getApplications: (state) => state.applications,
    getLoadingDataWaiting: (state) => state.loadingDataWaiting
  },

  mutations: {
    SET_APPLICATIONS(state, value){
      state.applications = value
    },
    LOADING_DATA_WAITING(state, value){
      state.loadingDataWaiting = value
    },
  },

  actions: {
    applications({commit}, page){
      const params = new URLSearchParams();
      params.append("per_page", 10);
      page && params.append("page", page);
      axios.get(`user/applications`,  { params, headers: {'Authorization': `Bearer ${localStorage.getItem('token')}`}})
      .then(res => {
        commit("LOADING_DATA_WAITING", false)
        commit("SET_APPLICATIONS", res.data)
      })
      .catch(()=> commit("LOADING_DATA_WAITING", false))
    },
    loadingDataWaiting({commit}, value){
      commit("LOADING_DATA_WAITING", value)
    },
  }
}
