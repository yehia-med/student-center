import App from "./App.vue";
import { createApp } from "vue";

// Plugins
import { registerPlugins } from "@/plugins";

import "./assets/app.scss";

const app = createApp(App);

registerPlugins(app);

app.mount("#app");
