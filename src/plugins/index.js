/**
 * plugins/index.js
 *
 * Automatically included in `./src/main.js`
 */

// Plugins

// router
import router from "../router";

// store
import store from "../store";

// PrimeVue
import PrimeVue from "primevue/config";
import "primevue/resources/themes/lara-light-indigo/theme.css";
import "primevue/resources/primevue.min.css";
// primeicons
import "primeicons/primeicons.css";
// primeflex
import "primeflex/primeflex.css";

// i18n
import i18n from "./i18n";

// axios
import axios from "axios";
// vue axios
import VueAxios from "vue-axios";

import Toast from "vue-toastification";
// Import the CSS or use your own!
import "vue-toastification/dist/index.css";

// moment
import {VueMoment} from 'vue-moment';
import moment from 'moment-timezone'
import ConfirmationService from 'primevue/confirmationservice';
const options = {
  draggable: false
};


// axios config
axios.defaults.baseURL = "https://backend.swift-lead.com/api/";
axios.defaults.headers.common["Accept-Language"] = "en";

export function registerPlugins(app) {
  app.use(router).use(store).use(PrimeVue).use(i18n).use(VueAxios, axios).use(Toast, options).use(VueMoment, {moment}).use(ConfirmationService);
}
