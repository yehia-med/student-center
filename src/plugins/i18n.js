import { createI18n } from "vue-i18n";

export default createI18n({
  locale: "ar", // set locale
  fallbackLocale: "en", // set fallback locale

  messages: {
    ar: {
      test: "تجربة"
    },

    en: {
      test: "test"
    }
  }
});
