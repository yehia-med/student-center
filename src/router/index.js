import { createRouter, createWebHistory } from "vue-router";

// import routes from modules
// public routes
import publicRoutes from "../modules/public/routes/public.routes.js";

// client account routes
import clientAccountRoutes from "../modules/client-account/routes/client-account.routes.js";

const baseRoutes = [];

const routes = baseRoutes.concat(
  // public routes
  publicRoutes,

  // client account routes
  clientAccountRoutes
);

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),

  routes,

  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      // return smooth scroll to top
      return { top: 0, behavior: "smooth" };
    }
  }
});

export default router;
