// client account routes
let userAccountRoutes = [
  // client account layout
  {
    path: "/",
    name: "ClientAccountLayout",
    component: () => import("../layout/ClientAccountLayout.vue"),
    redirect: { name: "AccountDocuments" },
    beforeEnter: (to, from, next) => {
      // if user is authenticated and role has permission to access dashboard
      localStorage.getItem('token')
        ? next()
        : next({ name: "Home" });
    },

    children: [
      // document
      {
        path: "account",
        name: "AccountDocuments",
        component: () => import("../views/AccountDocuments.vue")
      },

      // Applications
      {
        path: "applications",
        name: "AccountApplications",
        component: () => import("../views/AccountApplications.vue")
      },

      // My Ads
      {
        path: "/",
        name: "AccountAds",
        component: () => import("../views/AccountAds.vue"),
        redirect: { name: "AdsAccommodations" },
        children: [
          {
            path: "ads-accommodations",
            name: "AdsAccommodations",
            component: () => import("../views/AdsAccommodations.vue")
          },
          {
            path: "ads-sell-buy",
            name: "AdsSellBuy",
            component: () => import("../views/AdsSellBuy.vue")
          },
          {
            path: "ads-workshops",
            name: "AdsWorkshops",
            component: () => import("../views/AdsWorkshops.vue")
          },
          {
            path: "ads-questions-answers",
            name: "AdsQuestionsAnswers",
            component: () => import("../views/AdsQuestionsAnswers.vue")
          }
        ]
      }
    ]
  }
];

export default userAccountRoutes;
