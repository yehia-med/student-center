// public routes
let publicRoutes = [
  // public layout
  {
    path: "/",
    name: "PublicLayout",
    component: () => import("../layout/PublicLayout.vue"),
    children: [
      // home
      {
        path: "",
        name: "Home",
        component: () => import("../views/home/Home.vue")
      },

      // university cources
      {
        path: "university-cources",
        name: "UniversityCourses",
        component: () => import("../views/cources/university/UniversityCourses.vue")
      },

      // english cources
      {
        path: "english-cources",
        name: "EnglishCourses",
        component: () => import("../views/cources/english/EnglishCourses.vue")
      },

      // about
      {
        path: "about-us",
        name: "About",
        component: () => import("../views/about/About.vue")
      },

      // contact
      {
        path: "contact-us",
        name: "Contact",
        component: () => import("../views/contact/Contact.vue")
      },

      // university cource profile
      {
        path: "university-cources/course/:id",
        name: "UniversityCourseProfile",
        component: () => import("../views/course-profile/university/UniversityCourseProfile.vue"),
      },

      // english cource profile
      {
        path: "english-cources/course/:id",
        name: "EnglishCourseProfile",
        component: () => import("../views/course-profile/english/EnglishCourseProfile.vue")
      },

      // terms & conditions
      {
        path: "terms-conditions",
        name: "Terms",
        component: () => import("../views/terms/Terms.vue")
      },
      
      {
        path: "privacy-policy",
        name: "Privacy",
        component: () => import("../views/terms/Privacy.vue")
      },

      /////////// accommodations ////////////
      {
        path: "accommodations",
        name: "Accommodations",
        component: () => import("../views/accommodations/Accommodations.vue")
      },

      // accommodation profile
      {
        path: "accommodation/:id",
        name: "AccommodationProfile",
        component: () => import("../views/accommodations/AccommodationProfile.vue")
      },

      // post accommodation
      {
        path: "post-accommodation",
        name: "PostAccommodation",
        component: () => import("../views/accommodations/PostAccommodation.vue")
      },

      //////////// sell & buy /////////////
      {
        path: "sell-buy",
        name: "SellBuy",
        component: () => import("../views/sell-buy/SellBuy.vue")
      },

      // sell & buy profile
      {
        path: "sell-buy/:id",
        name: "SellBuyProfile",
        component: () => import("../views/sell-buy/SellBuyProfile.vue")
      },

      // post sell and buy
      {
        path: "post-sell-and-buy",
        name: "PostSellAndBuy",
        component: () => import("../views/sell-buy/PostSellAndBuy.vue")
      },

      ///////////// workshop /////////////
      {
        path: "workshops-tutors",
        name: "Workshops",
        component: () => import("../views/workshops/Workshops.vue")
      },

      // workshop profile
      {
        path: "workshop/:id",
        name: "WorkshopProfile",
        component: () => import("../views/workshops/WorkshopProfile.vue")
      },

      // post workshop and tutors
      {
        path: "post-workshop-and-tutors",
        name: "PostWorkshopAndTutors",
        component: () => import("../views/workshops/PostWorkshopAndTutors.vue")
      },

      /////////// Questions & Answers //////////
      {
        path: "questions-answers",
        name: "QuestionsAnswers",
        component: () => import("../views/questions-answers/QuestionsAnswers.vue")
      },

      // Post Question
      {
        path: "post-question",
        name: "PostQuestion",
        component: () => import("../views/questions-answers/PostQuestion.vue")
      },

      // blog
      {
        path: "blog",
        name: "Blog",
        component: () => import("../views/blog/Blog.vue")
      },

      // article
      {
        path: "blog/:id",
        name: "Article",
        component: () => import("../views/blog/Article.vue")
      },

      // quick apply
      {
        path: "quick-apply",
        name: "QuickApply",
        component: () => import("../views/quick-apply/QuickApply.vue")
      },

    ]
  }
];

export default publicRoutes;
